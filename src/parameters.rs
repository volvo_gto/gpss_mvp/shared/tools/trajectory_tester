use r2r::ParameterValue;
use std::collections::HashMap;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize, Default)]
pub struct ControllerParameters {
    pub atr_id: i16,
    pub frequency: f64,
    pub linear_velocity: f64,
    pub turning_velocity: f64,
    pub wheel_track: f64,
    pub turning_radius: f64,
    pub allow_rotation: bool,
    pub speed_scale: f64,    // used for tuning the model if the real robot is faster or slower
    pub simulation: bool,  // in simulation mode, we dont care about getting new observations from the cameras.
}

/// Initialize parameters given a set of ros parameters
pub fn init_parameters(ros_params: HashMap<String, ParameterValue>) -> ControllerParameters {
    ControllerParameters {
        atr_id: ros_params.get("atr_id").and_then(get_i16).unwrap_or(16),
        frequency: ros_params.get("frequency").and_then(get_f64).unwrap_or(20.0),
        linear_velocity: ros_params.get("linear_velocity").and_then(get_f64).unwrap_or(0.5),
        turning_velocity: ros_params.get("turning_velocity").and_then(get_f64).unwrap_or(0.55),
        wheel_track: ros_params.get("wheel_track").and_then(get_f64).unwrap_or(0.48),
        turning_radius: ros_params.get("turning_radius").and_then(get_f64).unwrap_or(1.0),
        allow_rotation: ros_params.get("allow_rotation").and_then(get_bool).unwrap_or(true),
        speed_scale: ros_params.get("speed_scale").and_then(get_f64).unwrap_or(1.00),
        simulation: ros_params.get("simulation").and_then(get_bool).unwrap_or(true), // TODO: notice true here!
    }
}

/// Given the old state, a new parameter, and a new value, return a new parameters object if
/// it has changed
pub fn update_parameter(parameters: ControllerParameters,
                        param_name: &str, param_val: ParameterValue) -> Option<ControllerParameters> {
    // Only a few parameters can be changed. (Frequency, atr_id etc cannot)
    if param_name == "linear_velocity" {
        if let Some(f) = get_f64(&param_val) {
            if f != parameters.linear_velocity {
                let mut new_params = parameters.clone();
                new_params.linear_velocity = f;
                println!("Changing parameter {} to {}", param_name, f);
                return Some(new_params);
            }
        }
    }
    if param_name == "turning_velocity" {
        if let Some(f) = get_f64(&param_val) {
            if f != parameters.turning_velocity {
                let mut new_params = parameters.clone();
                new_params.turning_velocity = f;
                println!("Changing parameter {} to {}", param_name, f);
                return Some(new_params);
            }
        }
    }
    if param_name == "turning_radius" {
        if let Some(f) = get_f64(&param_val) {
            if f != parameters.turning_radius {
                let mut new_params = parameters.clone();
                new_params.turning_radius = f;
                println!("Changing parameter {} to {}", param_name, f);
                return Some(new_params);
            }
        }
    }
    if param_name == "rotation" {
        if let Some(f) = get_bool(&param_val) {
            if f != parameters.allow_rotation {
                let mut new_params = parameters.clone();
                new_params.allow_rotation = f;
                println!("Changing parameter {} to {}", param_name, f);
                return Some(new_params);
            }
        }
    }
    // no change.
    println!("Not changing parameter {}", param_name);
    return None;
}

fn get_i16(ros_param: &ParameterValue) -> Option<i16> {
    if let r2r::ParameterValue::Integer(i) = ros_param {
        Some((*i).try_into().expect("overflow atr_id"))
    } else {
        None
    }
}

fn get_f64(ros_param: &ParameterValue) -> Option<f64> {
    if let r2r::ParameterValue::Double(f) = ros_param {
        Some(*f)
    } else {
        None
    }

}fn get_bool(ros_param: &ParameterValue) -> Option<bool> {
    if let r2r::ParameterValue::Bool(b) = ros_param {
        Some(*b)
    } else {
        None
    }
}
