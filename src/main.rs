use r2r;
use r2r::gpss_control_interfaces::msg::{SpeedAct, SpeedRef};
use r2r::atr_object_msgs::msg::ObjectStampedList;
use r2r::atr_object_msgs::msg::ObjectListStamped;
use r2r::gpss_control_interfaces::msg::ControllerState as RosControllerState;
use r2r::gpss_control_interfaces::msg::Waypoint;
use r2r::sensor_msgs::msg::JointState;
use r2r::geometry_msgs::msg::Point;
use r2r::scene_manipulation_msgs::srv::LookupTransform;
use futures::stream::{Stream, StreamExt};
use std::f64::consts::PI;
use std::time::{Duration, Instant};
use std::sync::{Arc, Mutex};
use std::collections::{VecDeque, HashMap};

mod math;
use math::*;

mod parameters;
use parameters::*;

#[derive(Debug, Clone, Default, PartialEq)]
pub struct ATRPose {
    x: f64,
    y: f64,
    theta: f64,
}

#[derive(Debug, Clone, Default)]
pub struct ATRObservation {
    pose: ATRPose,
    timestamp: Duration,
}

#[derive(Debug, Clone, Default)]
pub struct Polygons {
    id: i16,
    polys: Vec<geo::Polygon>,
    timestamp: Duration,
}


#[derive(Debug, Clone, Default)]
pub struct ControllerState {
    // tunable parameters
    parameters: ControllerParameters,
    // running = false -> stop.
    running: bool,
    // current ATR pose
    atr_pose: ATRPose,
    // last observation from vision system
    last_observation: ATRObservation,
    // current list of waypoints to visit
    to_visit: VecDeque<Waypoint>,
    // time to reach remaining waypoints
    time_to_waypoint: Vec<f64>,
    // active waypoint, if there is one
    current_waypoint: Option<Waypoint>,
    // is current waypoint terminal?
    current_waypoint_terminal: bool,
    // last reached waypoint
    last_reached_waypoint: Option<Waypoint>,
    // when the last waypoint was reached
    last_reached_waypoint_timestamp: Option<Instant>,
    // blocked due to collision?
    blocked: bool,
    // blocked due to no camera observation?
    no_observation: bool,
    // if we have currently run a path to the end.
    path_completed: bool,
    // active collision objects
    polygons: Vec<geo::Polygon>,
    collision_polygons: Polygons,
    collision_atrs: Vec<Polygons>,
    current_linear_velocity: f64
}


#[derive(Debug, Clone, Default)]
struct Vector{
    x: f64,
    y: f64
}

impl Vector{
    fn new(tail: &Point, head: &Point) -> Self {
        Vector { x: head.x - tail.x, y: head.y - tail.y }
    }
    fn length(&self) -> f64 {
        sqrt(self.x * self.x + self.y * self.y)
    }
    fn dot(&self, other: &Vector) -> f64 {
        self.x * other.x + self.y * other.y
    }
    fn angle(&self, other: &Vector) -> f64 {
        acos(self.dot(other) / (self.length()*other.length()) )
    }
    fn project(&self, on_to: &Vector) -> Vector {
        let scale = self.dot(on_to) / on_to.dot(on_to);
        Vector { x: on_to.x * scale, y: on_to.y * scale }
    }

}


// Compute the aiming point on the line between waypoints as next goal
// If returning true start moving towards next waypoint
fn compute_aiming_point(
    atr_pose: &ATRPose,
    radius: f64,
    start_wp: Option<&Point>,
    goal_wp: &Waypoint,
    next_wp: Option<&Point>,
    allow_rotation: bool
) -> (f64, f64, bool) {

    let atr_point = Point {x: atr_pose.x, y: atr_pose.y, z: 0.0};
    let atr_goal_vector = Vector::new(&atr_point, &goal_wp.point);
    let start_atr_vector = start_wp.map(|start| Vector::new(start, &atr_point));
    let start_goal_vector = start_wp.map(|start| Vector::new(start, &goal_wp.point));
    let next_vector = next_wp.map(|n| Vector::new(&goal_wp.point, n));

    // if we are moving towards the first waypoint, the current atr pose is the start
    let start_goal_vector = start_goal_vector.unwrap_or(atr_goal_vector.clone());

    // we are close, change wp
    if atr_goal_vector.length() < radius {
        return (goal_wp.point.x, goal_wp.point.y, next_wp.is_some())
    }

    // check when we need to start turning to allow for the minimum radius
    let distance_to_turn = next_vector.as_ref().map(|n_v|{
            let angle = atr_goal_vector.angle(&n_v);
            let x = tan(angle/2.0);
            // println!("ANGLE X: {}", x);
            if x == f64::NAN || x > 2.0 || x < 0.1 {
                // the angle is too sharp. Go to waypoint
                0.0
            } else {
                radius * x
            }
    }).unwrap_or(0.0);

    // let distance_to_turn = min(distance_to_turn, goal_wp.zone);


    // println!("d: {}, go goal: {}", distance_to_turn, atr_goal_vector.length());
    // println!("next: {:?}", next_vector);

    // should we aim for to_goal_vector or on the next_vector
    // we will change waypoint when we are at distance_to_turn + 0.2. O.2 is due to that we should turn before we passed
    // the point.
    if distance_to_turn + 0.1 < atr_goal_vector.length()  {  // to_goal_vector
        if let (Some(s_a_v), Some(start_point)) = (start_atr_vector, start_wp) {
            let proj = s_a_v.project(&start_goal_vector);
            let forward_scale = (1.5 + proj.length()) / start_goal_vector.length(); // aim 2*radius infront of atr
            if forward_scale > 1.0 || forward_scale == f64::NAN {
                return (goal_wp.point.x, goal_wp.point.y, false);
            }
            let aim_x = start_point.x + start_goal_vector.x * forward_scale;
            let aim_y = start_point.y + start_goal_vector.y * forward_scale;

            // DO not understand why we get NaN sometimes. Need to analyse better soon
            if aim_x == f64::NAN || aim_y == f64::NAN {
                return (goal_wp.point.x, goal_wp.point.y, false);
            }

            return (aim_x, aim_y, false)
        } else {
            return (goal_wp.point.x, goal_wp.point.y, false);
        }
    }
    else {
        // maybe we need to update an aiming point to follow the circle, but for now, let us just change to the next point and
        // let the controller set the rotation speed
        return (goal_wp.point.x, goal_wp.point.y, true);
    }
}




/// Compute the angle between error in heading from theta, the current
/// location and the goal location
fn compute_theta_error(atr_pose: &ATRPose, goal_x: f64, goal_y: f64) -> f64 {
    // ATR direction vector
    let x1 = cos(atr_pose.theta);
    let y1 = sin(atr_pose.theta);

    // goal vector
    let mut x2 = goal_x-atr_pose.x;
    let mut y2 = goal_y-atr_pose.y;
    // normalize
    let len = sqrt(x2*x2 + y2*y2);
    x2 /= len;
    y2 /= len;

    if len == 0.0 {
        return PI
    }

    // compute angle between the two vectors
    let dot = x1*x2 + y1*y2;
    let det = x1*y2 - y1*x2;
    let theta_error = atan2(det, dot);

    if theta_error == f64::NAN {
        return PI
    }

    theta_error
}

/// Compute the angle between error in heading from theta, the current
/// location and the goal location
fn compute_theta_error_terminal(atr_pose: &ATRPose, terminal_heading: f64) -> f64 {
    // ATR direction vector
    let x1 = cos(atr_pose.theta);
    let y1 = sin(atr_pose.theta);

    // terminal heading direction vector
    let x2 = cos(terminal_heading);
    let y2 = sin(terminal_heading);

    // compute angle between the two vectors
    let dot = x1*x2 + y1*y2;
    let det = x1*y2 - y1*x2;
    let theta_error = atan2(det, dot);

    theta_error
}

/// Compute desired wheel velocities. (in m/s per wheel)
fn compute_wheel_vel(forward_vel: f64,
                     heading_vel: f64,
                     wheel_track: f64) -> (f64, f64) {
    let forward_speed = forward_vel;
    let turning_speed = (wheel_track / 2.0)*(heading_vel);

    // compute left and right speeds to aim for the heading
    let left = forward_speed - turning_speed;
    let right = forward_speed + turning_speed;

    (left, right)
}

fn compute_lin_velocity(atr_pose: &ATRPose, goal: &Waypoint, theta_error: f64, radius: f64, ref_speed:f64, allow_rotation: bool) -> f64 {
    let d_g = distance(atr_pose.x, atr_pose.y, goal.point.x, goal.point.y);
    if allow_rotation && theta_error > PI/2.0 {
        return 0.0;
    }
    let speed = if d_g < radius {
        ref_speed * (d_g / radius) // slowing down when getting closer
    } else {
        ref_speed * (1.0-(0.7*abs(theta_error)/PI)) // slow down some when turning
    };
    return max(0.15, speed)
}

fn compute_theta_velocity(linear_vel: f64, theta_error: f64, radius: f64, allow_rotation: bool) -> f64 {
    0.8*theta_error
    // if allow_rotation && abs(theta_error) < PI*30.0/180.0 {
    //     0.8*theta_error
    // } else {
    //     sign(theta_error) * linear_vel / radius
    // }
}

/// Compute next state given wheel velocities (in m/s)
fn euler_step(atr_pose: &ATRPose, wheel_track: f64, step_time: f64,
              vl: f64, vr: f64) -> ATRPose {
    let forward_vel = (vl + vr) / 2.0;
    let x_d = forward_vel*cos(atr_pose.theta);
    let y_d = forward_vel*sin(atr_pose.theta);

    let theta_vel = (vr - vl) / wheel_track;

    // try to account for errors (friction, slipping, etc) by
    // linearly reducing how far we get vs the ideal model.
    // TODO: move to parameter if it seem to work well...
    const MODEL_ERROR: f64 = 1.0;
    // euler step
    ATRPose {
        x: atr_pose.x + x_d * step_time * MODEL_ERROR,
        y: atr_pose.y + y_d * step_time * MODEL_ERROR,
        theta: atr_pose.theta + theta_vel * step_time * MODEL_ERROR,
    }
}

/// Rewind and replay command buffer for 'steps' steps from the initial state.
fn replay_n_steps(atr_pose: &ATRPose, wheel_track: f64, step_time: f64,
                  steps: usize, command_buffer: &VecDeque::<SpeedRef>) -> ATRPose {
    let mut atr_pose = atr_pose.clone();
    let steps = std::cmp::min(steps, command_buffer.len());
    let cmdlen = command_buffer.len();
    for i in 1..steps {
        let vl = command_buffer[cmdlen-steps+i].ref_speed_left_wheel;
        let vr = command_buffer[cmdlen-steps+i].ref_speed_right_wheel;
        atr_pose = euler_step(&atr_pose, wheel_track, step_time, vl, vr);
    }
    atr_pose
}

/// Check for collisions between the current point and the next waypoint.
fn check_collision(
    atr_pose: &ATRPose,
    goal_x: f64,
    goal_y: f64,
    lin_vel: f64,
    next_wp: Option<&Point>,
    polygons: &[geo::Polygon]) -> bool {

    // in direction (angle) to the next waypoint
    let x = goal_x-atr_pose.x;
    let y = goal_y-atr_pose.y;
    let dist_to_waypoint = sqrt(x*x + y*y);
    let angle = atan2(y,x);

    let lock_ahead = 2.5; //lin_vel / 1.0 * 2.5 + 0.5;

    // create a polygon that starts 0.8m ahead of the ATR wheels, ends 1.6m
    // ahead in the direction of the waypoint and extends 0.4m on each
    // side.
    let dist_ahead_start = 0.8;
    let offset_x = 0.2;
    let dist_ahead_stop = min(lock_ahead, dist_to_waypoint);

    let remaining_dist_ahead = {
        if next_wp.is_some() && dist_to_waypoint < lock_ahead   {
            lock_ahead - dist_to_waypoint
        } else {
            0.0
        }
    };

    let (p1_x, p1_y) = rotate_point(dist_ahead_start, -offset_x, angle);
    let (p2_x, p2_y) = rotate_point(dist_ahead_stop, -offset_x, angle);
    let (p3_x, p3_y) = rotate_point(dist_ahead_stop, offset_x, angle);
    let (p4_x, p4_y) = rotate_point(dist_ahead_start, offset_x, angle);

    let mut waypoint_poly = vec![
            (atr_pose.x+p1_x, atr_pose.y+p1_y),
            (atr_pose.x+p2_x, atr_pose.y+p2_y),
            (atr_pose.x+p3_x, atr_pose.y+p3_y),
            (atr_pose.x+p4_x, atr_pose.y+p4_y)];

    // create a smaller polygon that extends in the direction
    // of the ATR.
    let offset_x = 0.5;
    let dist_ahead_stop = min(1.2, dist_to_waypoint);
    let angle = atr_pose.theta;
    let (p1_x, p1_y) = rotate_point(dist_ahead_start, -offset_x, angle);
    let (p2_x, p2_y) = rotate_point(dist_ahead_stop, -offset_x, angle);
    let (p3_x, p3_y) = rotate_point(dist_ahead_stop, offset_x, angle);
    let (p4_x, p4_y) = rotate_point(dist_ahead_start, offset_x, angle);

    let atr_poly = geo::Polygon::new(
        geo::LineString::from(vec![
            (atr_pose.x+p1_x, atr_pose.y+p1_y),
            (atr_pose.x+p2_x, atr_pose.y+p2_y),
            (atr_pose.x+p3_x, atr_pose.y+p3_y),
            (atr_pose.x+p4_x, atr_pose.y+p4_y)]),
        vec![],
    );

    // if we have a second waypoint and extend across the current waypoint,
    // extend out polygon towards the next
    if remaining_dist_ahead > 0.0 {
        let next_wp = next_wp.unwrap();
        let next_x = next_wp.x;
        let next_y = next_wp.y;

        let xx = next_x-goal_x;
        let yy = next_y-goal_y;
        let angle = atan2(yy,xx);

        let (p1_x, p1_y) = rotate_point(dist_ahead_start, -offset_x, angle);
        let (p2_x, p2_y) = rotate_point(dist_ahead_stop, -offset_x, angle);
        let (p3_x, p3_y) = rotate_point(dist_ahead_stop, offset_x, angle);
        let (p4_x, p4_y) = rotate_point(dist_ahead_start, offset_x, angle);

        waypoint_poly.push((goal_x+p1_x, goal_y+p1_y));
        waypoint_poly.push((goal_x+p2_x, goal_y+p2_y));
        waypoint_poly.push((goal_x+p3_x, goal_y+p3_y));
        waypoint_poly.push((goal_x+p4_x, goal_y+p4_y));
    }

    

    let mut waypoint_poly = geo::Polygon::new(
        geo::LineString::from(waypoint_poly),
        vec![],
    );

    use geo::Intersects;
    polygons.iter().any( |p| { 
        let x = waypoint_poly.intersects(p) || atr_poly.intersects(p);

        // if x {
        //     println!("COLL*******");
        //     println!("p: {:?}", &p);
        //     println!("waypoint_poly: {:?}", &waypoint_poly);
        //     println!("a: {:?}", &atr_poly);
        //     println!("COLL///////");

        // }
        
        x
    })
}

/// Compute collision polygon for this ATR
fn compute_collision_object(
    atr_id: i16, 
    atr_pose: &ATRPose, 
    goal_x: f64, 
    goal_y: f64,
    lin_vel: f64,
    next_goal_x: Option<f64>, 
    next_goal_y: Option<f64>) -> r2r::atr_object_msgs::msg::Object {
    use r2r::geometry_msgs::msg::Point32;
    // in direction (angle) to the next waypoint
    let x = goal_x-atr_pose.x;
    let y = goal_y-atr_pose.y;
    let dist_to_waypoint = sqrt(x*x + y*y);

    let angle = if x.abs() < 0.1 && y.abs() < 0.1 {
        // if we are at the goal, use current atr angle
        atr_pose.theta
    } else {
        // if not, use mean of current angle and error to smooth it a bit.
        let theta_error = compute_theta_error(&atr_pose, goal_x, goal_y);
        atan2(y,x) - 0.7 * theta_error
    };

    let dist_ahead_start = -0.2;
    let offset_x = 0.5;
    let dist_ahead = lin_vel / 1.0 * 2.5 + 0.5;
    let dist_ahead_stop = if x.abs() < 0.1 && y.abs() < 0.1 {
        0.8
    } else {
        min(dist_ahead, dist_to_waypoint)
    };

    let (p1_x, p1_y) = rotate_point(dist_ahead_start, -offset_x, angle);
    let (p2_x, p2_y) = rotate_point(dist_ahead_stop, -offset_x, angle);
    let (p3_x, p3_y) = rotate_point(dist_ahead_stop, offset_x, angle);
    let (p4_x, p4_y) = rotate_point(dist_ahead_start, offset_x, angle);

    let p1 = Point32 { x: (atr_pose.x+p1_x) as f32, y: (atr_pose.y+p1_y) as f32, z: 0.0 };
    let p2 = Point32 { x: (atr_pose.x+p2_x) as f32, y: (atr_pose.y+p2_y) as f32, z: 0.0 };
    let p3 = Point32 { x: (atr_pose.x+p3_x) as f32, y: (atr_pose.y+p3_y) as f32, z: 0.0 };
    let p4 = Point32 { x: (atr_pose.x+p4_x) as f32, y: (atr_pose.y+p4_y) as f32, z: 0.0 };

    let mut polygon = r2r::geometry_msgs::msg::Polygon {
        points: vec![p1, p2, p3, p4],
    };

    // if we have a second waypoint and extend across the current waypoint,
    // extend out polygon towards the next
    if next_goal_x.is_some() && next_goal_y.is_some() {
        let next_x = next_goal_x.unwrap();
        let next_y = next_goal_y.unwrap();

        let xx = next_x-goal_x;
        let yy = next_y-goal_y;
        let angle = atan2(yy,xx);

        let remaining_dist_ahead = dist_ahead - dist_to_waypoint;

        let dist_ahead_start = 0.0;
        let dist_ahead_stop = remaining_dist_ahead;
        if dist_ahead_stop > 0.0 {
            let (p1_x, p1_y) = rotate_point(dist_ahead_start, -offset_x, angle);
            let (p2_x, p2_y) = rotate_point(dist_ahead_stop, -offset_x, angle);
            let (p3_x, p3_y) = rotate_point(dist_ahead_stop, offset_x, angle);
            let (p4_x, p4_y) = rotate_point(dist_ahead_start, offset_x, angle);

            let p1 = Point32 { x: (goal_x+p1_x) as f32, y: (goal_y+p1_y) as f32, z: 0.0 };
            let p2 = Point32 { x: (goal_x+p2_x) as f32, y: (goal_y+p2_y) as f32, z: 0.0 };
            let p3 = Point32 { x: (goal_x+p3_x) as f32, y: (goal_y+p3_y) as f32, z: 0.0 };
            let p4 = Point32 { x: (goal_x+p4_x) as f32, y: (goal_y+p4_y) as f32, z: 0.0 };

            polygon.points.extend(vec![p1, p2, p3, p4]);
        }
    }

    let object = r2r::atr_object_msgs::msg::Object {
        polygon,
        object_c: r2r::atr_object_msgs::msg::ObjectClass { o_class: 7 }, // ATR
        object_id: atr_id,
        .. r2r::atr_object_msgs::msg::Object::default()
    };

    object
}

fn saturate_wheel_velocity(vl: &mut f64, vr: &mut f64) {
    // saturate outputs
    let max_wheel_vel = 0.8; // TODO: parameter?
    if *vl > max_wheel_vel {
        *vl = max_wheel_vel;
    }
    if *vl < -max_wheel_vel {
        *vl = -max_wheel_vel;
    }
    if *vr > max_wheel_vel {
        *vr = max_wheel_vel;
    }
    if *vr < -max_wheel_vel {
        *vr = -max_wheel_vel;
    }
}

async fn tick(p: r2r::Publisher<SpeedRef>,
              mut t: r2r::Timer,
              shared_state: Arc<Mutex<ControllerState>>) -> Result<(), Box<dyn std::error::Error>> {
    let initial_state = { (*shared_state.lock().unwrap()).clone() };
    let mut atr_pose = initial_state.atr_pose.clone();

    let period = 1.0 / initial_state.parameters.frequency;
    let vel_ramp = 1.5 * 0.25 * period;
    let theta_vel_ramp = 1.5 * period;

    let control_delay = Duration::from_secs_f64(0.3);

    // wheel speed ramping
    let mut vl = 0.0;
    let mut vr = 0.0;

    let mut final_lin_v = 0.0;
    let mut final_theta_v = 0.0;


    let mut clock = r2r::Clock::create(r2r::ClockType::RosTime)?;
    let now = clock.get_now()?;
    let _tick_time = r2r::Clock::to_builtin_time(&now);

    // testing
    let mut prev_tag_pose: ATRPose = atr_pose.clone();
    let mut prev_tag_time = now - Duration::from_secs_f64(2.0);
    let mut prev_tick_pose = atr_pose.clone();
    let mut prev_tick_time = now - Duration::from_secs_f64(2.0);


    let mut prev_observation = now - Duration::from_secs_f64(2.0);

    let command_buffer_size = (5.0 * initial_state.parameters.frequency) as usize;
    let mut command_buffer = VecDeque::<SpeedRef>::
    with_capacity(command_buffer_size);

    let clear_time = 2.0f64;
    let mut clear_counter = 0;

    let mut error_integral = 0.0;


    loop {
        let _elapsed = t.tick().await?;

        let now = clock.get_now()?;

        let parameters = {
            shared_state.lock().unwrap().parameters.clone()
        };
        let wheel_track = parameters.wheel_track;
        let period = 1.0 / parameters.frequency;

        // Check if there is a new observation
        let last_observation = {
            shared_state.lock().unwrap().last_observation.clone()
        };
        if prev_observation < last_observation.timestamp {
            prev_observation = last_observation.timestamp;

            atr_pose = last_observation.pose;
            // println!("Updated state from camera {}", atr_pose);
            {
                shared_state.lock().unwrap().no_observation = false;
            }

            // If needed, rewind commmand buffer
            // and simulate forward from this point.
            let now = clock.get_now()?;
            if last_observation.timestamp > now {
                println!("Skipping dead reckoning, timestamp from future");
                println!("{}ms", (last_observation.timestamp - now).as_millis());
            } else {
                // Testing to change the clock to emulate a delay of control
                let time_passed = now - (last_observation.timestamp - control_delay);
                let time_passed = time_passed.as_secs_f64();
                let rewind_steps = (time_passed / period) as usize;
                let x = replay_n_steps(&atr_pose, wheel_track, period,
                                          rewind_steps, &command_buffer);


                let ob_point = Point{x: atr_pose.x, y: atr_pose.y, z: 0.0};
                let prev_point = Point{x: prev_tag_pose.x.clone(), y: prev_tag_pose.y.clone(), z: 0.0};
                let v = Vector::new(&prev_point, &ob_point);
                // println!("");
                // println!("TAG: x:{}, y:{}, t:{}", atr_pose.x - x.x, atr_pose.y - x.y, atr_pose.theta - x.theta);
                // println!("rewind: {}, {}s", rewind_steps, time_passed);
                // println!("TAG speed: {} m/s", v.length()/(last_observation.timestamp - prev_tag_time).as_secs_f64());
                // println!("");
                prev_tag_time = last_observation.timestamp.clone();
                prev_tag_pose = atr_pose.clone();


                atr_pose = x;

            }
        } else {
            // No new observation, check if we should stand still...
            let now = clock.get_now()?;
            let deadline = Duration::from_millis(15000);
            if !parameters.simulation && (now - last_observation.timestamp) > deadline {
                println!("No camera update for {}ms, stopping", Duration::from_millis(15000).as_millis());
                shared_state.lock().unwrap().no_observation = true;
            }
        }

        let (current_waypoint, current_waypoint_terminal, last_waypoint, next_waypoint) = {
            let ss = shared_state.lock().unwrap();
            (ss.current_waypoint.clone(), ss.current_waypoint_terminal.clone(), ss.last_reached_waypoint.clone(), ss.to_visit.front().cloned())
        };

        let mut theta_vel = 0.0;
        let mut linear_vel = 0.0;
        if let Some(current_waypoint) = current_waypoint {
            // We are going somewhere...

            //testing
            let ob_point = Point{x: atr_pose.x, y: atr_pose.y, z: 0.0};
            let prev_point = Point{x: prev_tick_pose.x.clone(), y: prev_tick_pose.y.clone(), z: 0.0};
            let v = Vector::new(&prev_point, &ob_point);
            // println!("");
            // println!("Tick speed: {} m/s", v.length()/_elapsed.as_secs_f64());
            // println!("");
            prev_tick_pose = atr_pose.clone();

            let goal_x = current_waypoint.point.x;
            let goal_y = current_waypoint.point.y;

            let goal_radius = current_waypoint.zone;
            let goal_direction = current_waypoint.direction;

            let start = last_waypoint.as_ref().map(|x| &x.point);
            let next = next_waypoint.as_ref().map(|x| &x.point);
            let (aim_x, aim_y, mut done) = compute_aiming_point(
                &atr_pose,
                parameters.turning_radius,
                start,
                &current_waypoint,
                next,
                parameters.allow_rotation);

            let theta_error = compute_theta_error(&atr_pose, aim_x, aim_y);
            let dist_error = distance(atr_pose.x, atr_pose.y, goal_x, goal_y);

            // println!("theta error: {}", theta_error);
            // println!("atr:{},{}, aim:{},{}, goal:{},{}", atr_pose.x, atr_pose.y, aim_x, aim_y, goal_x, goal_y);

            linear_vel = compute_lin_velocity(&atr_pose, &current_waypoint, theta_error, parameters.turning_radius, parameters.linear_velocity, parameters.allow_rotation);
            theta_vel = compute_theta_velocity(linear_vel, theta_error, parameters.turning_radius, parameters.allow_rotation);

            // println!("lin: {}, theta: {}", linear_vel, theta_vel);


            if parameters.allow_rotation && current_waypoint_terminal && dist_error < 0.3 {
                // linear_vel = linear_vel * (dist_error / 0.8);
                // make sure we stop
                if dist_error < 0.3 {
                    linear_vel = 0.0;
                    let te = compute_theta_error_terminal(&atr_pose, goal_direction);
                    theta_vel = 0.25*te + 0.03 * error_integral;
                    error_integral += te * period;
                    if te * error_integral < 0.0 {
                        error_integral = 0.0;
                    }
                    if abs(te) < 0.07 {
                        error_integral = 0.0;
                        done = true;
                    }
                }
            } else if !parameters.allow_rotation && current_waypoint_terminal && dist_error < 0.3 {
                linear_vel = 0.0;
                done = true;
            }
            // else if !current_waypoint_terminal && dist_error < goal_radius {
            //     done = true;
            // }


            // if theta_error * 180.0 / PI > 70.0 {
            //     linear_vel = 0.0;
            // }

            if done {
                let mut ss = shared_state.lock().unwrap();
                ss.last_reached_waypoint = Some(current_waypoint);
                ss.last_reached_waypoint_timestamp = Some(Instant::now());
                ss.current_waypoint = ss.to_visit.pop_front();
                ss.current_waypoint_terminal = ss.to_visit.len() == 0;

                // set path completed when we finish.
                if let Some(wp) = ss.current_waypoint.as_ref() {
                    println!("|*****************************|");
                    println!("|*****************************|");
                    println!("starting on new waypoint: {}", wp.id);
                    println!("|*****************************|");
                    println!("|*****************************|");
                    println!("|*****************************|");
                    println!("|*****************************|");
                    println!("|*****************************|");
                } else {
                    println!("path completed!");
                    ss.path_completed = true;
                }
            }

            // Check collisions if we got new data. (TODO timestamp check)
            {
                let polygons = shared_state.lock().unwrap().polygons.clone();
                let collision = check_collision(&atr_pose, goal_x, goal_y, linear_vel, next, &polygons);
                if collision {
                    // stop!
                    println!("collision imminent! stopping");
                    {
                        let ss = shared_state.lock().unwrap();
                        println!("ATR blocking: {}, {}, {}", ss.collision_atrs.len(), ss.collision_polygons.polys.len(), ss.polygons.len());
                    }

                    // set linear velocity to 0. we still allow turning in
                    // order to get back to a previous waypoint.
                    linear_vel = 0.0;

                    // we need to be clear for 'clear_time' seconds before
                    // we allow new motions.
                    clear_counter = (clear_time / period) as i64;
                    shared_state.lock().unwrap().blocked = true;
                } else {
                    if clear_counter > 0 {
                        // still not allowed to move
                        println!("waiting to start after collision...");
                        linear_vel = 0.0;
                        clear_counter -= 1;
                    } else {
                        shared_state.lock().unwrap().blocked = false;
                    }
                }
            }
        }

        {
            // if not running then dont run
            if !(*shared_state.lock().unwrap()).running {
                // println!("not running...");
                linear_vel = 0.0;
                theta_vel = 0.0;
            }

            // if no observation, dont run
            // if (*shared_state.lock().unwrap()).no_observation {
            //     // println!("not running...");
            //     linear_vel = 0.0;
            //     theta_vel = 0.0;
            // }
        }


        // test to ramp differently on lin and heading
        if abs(linear_vel - final_lin_v) > vel_ramp {
            final_lin_v += vel_ramp*sign(linear_vel - final_lin_v);
        } else {
            final_lin_v = linear_vel;
        }

        if abs(theta_vel - final_theta_v) > theta_vel_ramp {
            final_theta_v += theta_vel_ramp*sign(theta_vel - final_theta_v);
        } else {
            final_theta_v = theta_vel;
        }


        let (mut vl, mut vr) = compute_wheel_vel(final_lin_v, final_theta_v,
                                                 parameters.wheel_track);

        // ramp up/down.
        // if abs(new_vl - vl) > vel_ramp {
        //     vl += vel_ramp*sign(new_vl - vl);
        // } else {
        //     vl = new_vl;
        // }
        // if abs(new_vr - vr) > vel_ramp {
        //     vr += vel_ramp*sign(new_vr - vr);
        // } else {
        //     vr = new_vr;
        // }

        saturate_wheel_velocity(&mut vl, &mut vr);

        // publish wheel speeds.
        let mut msg = SpeedRef::default();
        msg.ref_speed_left_wheel = vl;
        msg.ref_speed_right_wheel = vr;

        // add joint command to command buffer,
        // removing the oldest command if buffer is full.
        if command_buffer.len() == command_buffer_size {
            let _first = command_buffer.pop_front();
        }
        command_buffer.push_back(msg.clone());

        // testing scaling the speed if the ATR is faster then expected
        // can also be used later for adaptive control
        msg.ref_speed_left_wheel *= parameters.speed_scale;
        msg.ref_speed_right_wheel *= parameters.speed_scale;

        if !parameters.simulation {
            // in simulation mode, don't move the wheels
            p.publish(&msg)?;
        }

        // check if teleport has moved the atr
        {
            let ss = shared_state.lock().unwrap();
            if atr_pose != ss.atr_pose {
                atr_pose = ss.atr_pose.clone();
            }

        }

        // update the atr pose based on the speed
        atr_pose = euler_step(&atr_pose, wheel_track, period, vl, vr);
        // println!("ATR state: {}", atr_pose);

        // update state
        {
            let mut ss = shared_state.lock().unwrap();
            ss.atr_pose = atr_pose.clone();

            // first element in list is seconds since we reached the previous waypoint
            // (minus sign for clarity only)
            let reached_s_ago = if let Some(instant) = ss.last_reached_waypoint_timestamp.as_ref() {
                let duration: Duration = Instant::now() - *instant;
                -duration.as_secs_f64()
            } else {
                0.0
            };
            let mut time_to_waypoint = vec![reached_s_ago];

            // given the current speed, compute time to reach the current waypoint.
            if let Some(wp) = ss.current_waypoint.as_ref() {
                let dist_error = distance(atr_pose.x, atr_pose.y, wp.point.x, wp.point.y);
                let speed = (vl + vr) / 2.0;
                time_to_waypoint.push(dist_error / speed);

                if ss.to_visit.len() > 0 {
                    let next = &ss.to_visit[0];
                    let dist = distance(wp.point.x, wp.point.y, next.point.x, next.point.y);
                    let time = dist / ss.parameters.linear_velocity;
                    time_to_waypoint.push(time);
                }
            }
            
            ss.current_linear_velocity = final_lin_v;

            // compute time to remaining waypoints using the max linear velocity
            // parameters.linear_velocity
            for i in 1..ss.to_visit.len() {
                let prev = &ss.to_visit[i-1];
                let this = &ss.to_visit[i];
                let dist = distance(prev.point.x, prev.point.y, this.point.x, this.point.y);
                let time = dist / ss.parameters.linear_velocity;
                time_to_waypoint.push(time);
            }
            ss.time_to_waypoint = time_to_waypoint;


            // update the collision polygons
            let old_d =  now - Duration::from_secs_f64(3.0);
            ss.collision_atrs.retain(|x| old_d < x.timestamp);
            ss.polygons = ss.collision_atrs.iter().flat_map(|x| x.polys.clone()).collect();
            if ss.collision_polygons.timestamp < old_d {
                ss.collision_polygons = Polygons::default();
            };
            let mut xs = ss.collision_polygons.polys.clone();
            ss.polygons.append(&mut xs);
        }
    }
}

/// Responsible for publishing the controller state to ROS
async fn controller_state_task(p: r2r::Publisher<RosControllerState>, mut t: r2r::Timer,
                               shared_state: Arc<Mutex<ControllerState>>) -> Result<(), Box<dyn std::error::Error>> {
    let mut clock = r2r::Clock::create(r2r::ClockType::RosTime)?;
    loop {
        let _elapsed = t.tick().await?;

        let state_msg = {
            let ss = shared_state.lock().unwrap();

            // create a path list where list[0] is the previous,
            // list[1] is the next, etc..
            let mut path = vec![];
            path.push(ss.last_reached_waypoint.as_ref().
                      map(|wp| wp.id.to_owned()).unwrap_or("none".to_string()));
            if let Some(current) = &ss.current_waypoint {
                path.push(current.id.to_owned());
            }
            path.extend(ss.to_visit.iter().map(|wp| wp.id.to_owned()));

            let pose = r2r::geometry_msgs::msg::Pose {
                position: r2r::geometry_msgs::msg::Point {
                    x: ss.atr_pose.x,
                    y: ss.atr_pose.y,
                    z: 0.0,
                },
                orientation: r2r::geometry_msgs::msg::Quaternion {
                    x: 0.0,
                    y: 0.0,
                    z: ss.atr_pose.theta,
                    w: 1.0,
                },
            };

            let status = if ss.no_observation {
                "blocked_no_camera_data"
            } else if ss.blocked {
                "blocked_collision"
            } else {
                "clear_to_go"
            };

            let now = clock.get_now().expect("could not get timestamp");
            let timestamp = r2r::Clock::to_builtin_time(&now);
            // publish state
            RosControllerState {
                timestamp,
                status: status.into(),
                pose,
                path,
                time_to_waypoint: ss.time_to_waypoint.clone(),
                running: ss.running,
                path_completed: ss.path_completed,
            }
        };
        p.publish(&state_msg)?;
    }
}

/// Responsible for publishing the collision object(s) of the ATR
async fn collision_object_pub_task(p: r2r::Publisher<ObjectListStamped>, mut t: r2r::Timer,
                                   shared_state: Arc<Mutex<ControllerState>>) -> Result<(), Box<dyn std::error::Error>> {
    loop {
        let _elapsed = t.tick().await?;

        let msg = {
            let ss = shared_state.lock().unwrap();
            let atr_id = ss.parameters.atr_id;

            let mut msg = ObjectListStamped::default();
            if let Some(wp) = ss.current_waypoint.as_ref() {
                let obj = if ss.to_visit.is_empty() {
                    compute_collision_object(atr_id, &ss.atr_pose, wp.point.x, wp.point.y, ss.current_linear_velocity, None, None)
                } else {
                    let next_x = ss.to_visit[0].point.x;
                    let next_y = ss.to_visit[0].point.y;
                    compute_collision_object(atr_id, &ss.atr_pose, wp.point.x, wp.point.y, ss.current_linear_velocity,  Some(next_x), Some(next_y))
                };
                msg.objects.push(obj);
            } else {
                let obj = compute_collision_object(atr_id, &ss.atr_pose, ss.atr_pose.x, ss.atr_pose.y, ss.current_linear_velocity,  None, None);
                msg.objects.push(obj);
            }
            msg.header.frame_id = "world".into();
            msg
        };
        p.publish(&msg)?;
    }
}

async fn joint_state_task(p: r2r::Publisher<JointState>, mut t: r2r::Timer,
                          shared_state: Arc<Mutex<ControllerState>>) -> Result<(), Box<dyn std::error::Error>> {
    let mut msg = JointState::default();
    let atr_id = { shared_state.lock().unwrap().parameters.atr_id };
    msg.name.push(format!("atr_{}_base_link_axis_x_joint", atr_id));
    msg.name.push(format!("atr_{}_base_link_axis_y_joint", atr_id));
    msg.name.push(format!("atr_{}_base_link_axis_z_joint", atr_id));
    msg.name.push(format!("atr_{}_back_right_wheel_joint", atr_id));
    msg.name.push(format!("atr_{}_back_left_wheel_joint", atr_id));
    msg.name.push(format!("atr_{}_sw_arm_right_connect_1", atr_id));
    msg.name.push(format!("atr_{}_front_right_swivel_wheel_joint", atr_id));
    msg.name.push(format!("atr_{}_sw_arm_left_connect_1", atr_id));
    msg.name.push(format!("atr_{}_front_left_swivel_wheel_joint", atr_id));

    msg.position = vec![0.0; 9];
    msg.velocity = vec![0.0; 9];
    msg.effort = vec![0.0; 9];

    msg.header.frame_id = "world".into();
    let mut clock = r2r::Clock::create(r2r::ClockType::RosTime)?;

    loop {
        let _elapsed = t.tick().await?;

        // publish current joint states
        let pose = { shared_state.lock().unwrap().atr_pose.clone() };

        msg.position[0] = pose.x;
        msg.position[1] = pose.y;
        msg.position[2] = pose.theta;

        // TODO velocity
        // TODO effort
        // joint_state_message_->effort.at(0) = state.wheel_vel(0);
        // joint_state_message_->effort.at(1) = state.wheel_vel(1);

        let now = clock.get_now()?;
        msg.header.stamp = r2r::Clock::to_builtin_time(&now);
        p.publish(&msg)?;
    }
}

impl std::fmt::Display for ATRPose {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "(x: {:.2}, y: {:.2}, t: {:.1})", self.x, self.y,
               self.theta * 180.0 / PI)
    }
}

async fn update_tags(mut sub: impl Stream<Item = ObjectStampedList> + Unpin,
                     shared_state: Arc<Mutex<ControllerState>>) {
    let atr_id = { shared_state.lock().unwrap().parameters.atr_id };
    //let mut x = 0;
    loop {
        if let Some(msg) = sub.next().await {
            // For testing ...
            // x+=1;
            // if x % 10 != 0 {
            //     continue;
            // }
            for msg in &msg.objects {
                if msg.object.object_c.o_class == 6 // TAG type
                    && msg.object.object_id == atr_id  // correct ATR id
                {
                    let timestamp = Duration::new(
                        msg.parent_stamp.sec.try_into().unwrap(),
                        msg.parent_stamp.nanosec.try_into().unwrap());

                    // transform tag point to agv point.

                    // offsets from center of tag to center between wheels
                    let atr_tag_x = 0.2;
                    let atr_tag_y = 0.2;

                    // transform offsets with theta angle
                    let tc = cos(msg.object.pose.orientation.z);
                    let ts = sin(msg.object.pose.orientation.z);
                    let x_offset = atr_tag_x * tc - atr_tag_y * ts;
                    let y_offset = atr_tag_x * ts + atr_tag_y * tc;

                    let new_observation = ATRObservation {
                        pose: ATRPose {
                            x: msg.object.pose.position.x + x_offset,
                            y: msg.object.pose.position.y + y_offset,
                            theta: msg.object.pose.orientation.z,
                        },
                        timestamp,
                    };
                    shared_state.lock().unwrap().last_observation =
                        new_observation;
                }
            }
        }
    }
}

async fn update_objects(mut sub: impl Stream<Item = ObjectListStamped> + Unpin,
                        shared_state: Arc<Mutex<ControllerState>>) -> Result<(), Box<dyn std::error::Error>>  {

    let mut clock = r2r::Clock::create(r2r::ClockType::RosTime)?;
    loop {
        if let Some(msg) = sub.next().await {
            // TODO: use timestamp later.
            let now = clock.get_now()?;

            let mut polygons = vec![];
            for objects in &msg.objects {
                if objects.polygon.points.len() > 2 // Polygon, add to our list
                {
                    let p = geometry_polygon_to_geo_polygon(&objects.polygon);
                    polygons.push(p);
                }
            }

            let mut ss = shared_state.lock().unwrap();
            ss.collision_polygons = Polygons{
                id: 0,
                polys: polygons,
                timestamp: now
            };
        }
    }
}

async fn update_atr_objects(mut sub: impl Stream<Item = ObjectListStamped> + Unpin,
                        shared_state: Arc<Mutex<ControllerState>>) -> Result<(), Box<dyn std::error::Error>>  {
    let atr_id = { shared_state.lock().unwrap().parameters.atr_id };
    let mut clock = r2r::Clock::create(r2r::ClockType::RosTime)?;

    loop {
        if let Some(msg) = sub.next().await {
            let mut ss = shared_state.lock().unwrap();
            let now = clock.get_now()?;
            for msg in &msg.objects {
                // exempt ourseleves
                if msg.object_c.o_class == 7 && msg.object_id == atr_id {
                    continue;
                }

                
                
                let mut polygons = vec![];
                if msg.polygon.points.len() > 2 && msg.object_c.o_class == 7 // Polygon, add to our list
                {
                    let p = geometry_polygon_to_geo_polygon(&msg.polygon);
                    polygons.push(p);
                }
                
                ss.collision_atrs.retain(|x| x.id != msg.object_id);
                ss.collision_atrs.push(Polygons{
                    id: msg.object_id,
                    polys: polygons,
                    timestamp: now,
                });

            }
        }
    }
}

async fn fetch_waypoints_tick(
    lookup_service: r2r::Client<r2r::scene_manipulation_msgs::srv::LookupTransform::Service>,
    shared_state: Arc<Mutex<ControllerState>>
) -> Result<(), Box<dyn std::error::Error>> {
    let mut interval = tokio::time::interval(tokio::time::Duration::from_millis(250));
    loop {
        interval.tick().await;

        // get current waypoint name, if any.
        let waypoint_frame = {
            if let Some(wp) = shared_state.lock()
                .unwrap().current_waypoint.as_ref() {
                    wp.id.to_owned()
                }
            else {
                continue;
            }
        };

        let msg = LookupTransform::Request {
            parent_frame_id: "world".to_string(),
            child_frame_id: waypoint_frame.clone(),
        };

        let request = lookup_service.request(&msg)
            .expect("could not make service request");

        let res = tokio::time::timeout(Duration::from_millis(100), request).await;

        if let Err(_) = res {
            println!("Scene manipulation service timeout...");
            continue;
        }

        let response = res.unwrap().unwrap();
        if !response.success {
            println!("Failed to look up {} in world.", waypoint_frame);
            continue;
        }

        // Update shared state with new transform.
        let noise = 0.1;
        {
            let mut ss = shared_state.lock().unwrap();
            if let Some(ref mut current_waypoint) = ss.current_waypoint.as_mut() {
                if !close_enough(
                    current_waypoint.point.x, 
                    current_waypoint.point.y, 
                    response.transform.transform.translation.x, 
                    response.transform.transform.translation.y,
                    noise)
                {
                    println!("Current waypoint moved!");

                    let q = nalgebra::Quaternion::from_vector(nalgebra::Vector4::new(
                        response.transform.transform.rotation.x,
                        response.transform.transform.rotation.y,
                        response.transform.transform.rotation.z,
                        response.transform.transform.rotation.w,
                    ));
                    (_, _, current_waypoint.direction) = nalgebra::UnitQuaternion::from_quaternion(q).euler_angles();
                    current_waypoint.point.x = response.transform.transform.translation.x;
                    current_waypoint.point.y = response.transform.transform.translation.y;
                }
            }
        }

        //
        // Also update the next waypoint, if we have one.
        //
        let waypoint_frame = {
            if let Some(wp) = shared_state.lock().unwrap().to_visit.front() {
                    wp.id.to_owned()
                }
            else {
                continue;
            }
        };

        let msg = LookupTransform::Request {
            parent_frame_id: "world".to_string(),
            child_frame_id: waypoint_frame.clone(),
        };

        let request = lookup_service.request(&msg)
            .expect("could not make service request");

        let res = tokio::time::timeout(Duration::from_millis(100), request).await;

        if let Err(_) = res {
            println!("Scene manipulation service timeout...");
            continue;
        }

        let response = res.unwrap().unwrap();
        if !response.success {
            println!("Failed to look up {} in world.", waypoint_frame);
            continue;
        }

        // Update shared state with new transform.
        {
            let mut ss = shared_state.lock().unwrap();
            if let Some(ref mut next_waypoint) = ss.to_visit.front_mut() {
                if next_waypoint.id == response.transform.child_frame_id && !close_enough(
                    next_waypoint.point.x, 
                    next_waypoint.point.y, 
                    response.transform.transform.translation.x, 
                    response.transform.transform.translation.y,
                    noise)
                {
                    println!("Next waypoint moved!");
                    next_waypoint.point.x = response.transform.transform.translation.x;
                    next_waypoint.point.y = response.transform.transform.translation.y;
                }
            }
        }
    }
}

fn close_enough(x: f64, y: f64, xnew: f64, ynew: f64, noise: f64) -> bool {
    abs(x-xnew) < noise && abs(y-ynew) < noise
}

/// Handle parameter changes
 async fn parameter_event_handler(mut events: impl Stream<Item = (String, r2r::ParameterValue)> + Unpin,
                                  shared_state: Arc<Mutex<ControllerState>>) {
    loop {
        if let Some((param_name, param_val)) = events.next().await {
            let params = shared_state.lock().unwrap().parameters.clone();
            if let Some(new_params) = update_parameter(params, &param_name, param_val) {
                (*shared_state.lock().unwrap()).parameters = new_params;
            }
        }
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let ctx = r2r::Context::create()?;
    let mut node = r2r::Node::create(ctx, "atr_controller", "")?;

    let parameters = init_parameters(node.params.lock().unwrap().clone());
    let atr_id = parameters.atr_id;
    let period = 1.0 / parameters.frequency;
    let shared_state = ControllerState {
        parameters,
        running: false,
        atr_pose: ATRPose {
            x: 11.0,
            y: 5.0,
            theta: 0.99,
        },
        last_observation: ATRObservation::default(),
        to_visit: VecDeque::new(),
        time_to_waypoint: Vec::new(),
        current_waypoint: None,
        current_waypoint_terminal: true,
        last_reached_waypoint: None,
        last_reached_waypoint_timestamp: None,
        blocked: false,
        no_observation: false,
        path_completed: true,
        polygons: vec!(),
        collision_polygons: Polygons { id: 0, polys: vec!(), timestamp: Duration::from_secs(0)},
        collision_atrs: vec!(),
        current_linear_velocity: 0.0

    };
    let shared_state = Arc::new(Mutex::new(shared_state));

    // agv locations from vision pipeline
    let tag_sub = node.subscribe::<ObjectStampedList>
        ("/area_0/global_ar_tags_fused", r2r::QosProfile::default())?;

    let (paramater_handler, parameter_events) = node.make_parameter_handler()?;
    tokio::task::spawn(paramater_handler);

    let shared_state_task = shared_state.clone();
    tokio::task::spawn(async move {
        parameter_event_handler(parameter_events, shared_state_task).await
    });

    let shared_state_task = shared_state.clone();
    tokio::task::spawn(async move {
        update_tags(tag_sub, shared_state_task).await
    });

    let object_sub = node.subscribe::<ObjectListStamped>
        ("/area_0/global_objects_fused", r2r::QosProfile::default())?;

        let shared_state_task = shared_state.clone();
        tokio::task::spawn(async move {
            update_objects(object_sub, shared_state_task).await.expect("fail");
        });

    let object_sub_atr = node.subscribe::<ObjectListStamped>
        ("/area_0/atr_zones", r2r::QosProfile::default())?;
    let shared_state_task = shared_state.clone();
    tokio::task::spawn(async move {
        update_atr_objects(object_sub_atr, shared_state_task).await.expect("fail");
    });

    // Control task.
    let pub_topic = format!("/atr_{}/cmd", atr_id);
    let joint_command_pub = node.create_publisher::<SpeedRef>
        (&pub_topic, r2r::QosProfile::default())?;
    let timer = node.create_wall_timer(Duration::from_secs_f64(period))?;
    let shared_state_task = shared_state.clone();
    tokio::task::spawn(async move {
        tick(joint_command_pub,
             timer, shared_state_task).await.expect("fail");
    });

    // Controller state task
    let pub_topic = format!("/atr_{}/controller_state", atr_id);
    let controller_state_pub = node.create_publisher::<RosControllerState>
        (&pub_topic, r2r::QosProfile::default())?;
    let timer = node.create_wall_timer(Duration::from_secs_f64(period))?;
    let shared_state_task = shared_state.clone();
    tokio::task::spawn(async move {
        controller_state_task(controller_state_pub,
                              timer, shared_state_task).await.expect("fail");
    });

    // Joint state publisher task.
    let pub_topic = format!("/joint_states_{}", atr_id);
    let joint_state_pub = node.create_publisher::<JointState>
        (&pub_topic, r2r::QosProfile::default())?;
    let timer = node.create_wall_timer(Duration::from_secs_f64(period))?;
    let shared_state_task = shared_state.clone();
    tokio::task::spawn(async move {
        joint_state_task(joint_state_pub, timer, shared_state_task).await.expect("fail");
    });

    // Collision object publisher task
    let pub_topic = format!("/area_0/atr_zones");
    let collision_object_pub = node.create_publisher::<ObjectListStamped>
        (&pub_topic, r2r::QosProfile::default())?;
    let timer = node.create_wall_timer(Duration::from_secs_f64(period))?;
    let shared_state_task = shared_state.clone();
    tokio::task::spawn(async move {
        collision_object_pub_task(collision_object_pub,
                                  timer, shared_state_task).await.expect("fail");
    });

    // "Run" subscriber
    let running_topic = format!("/atr_{}/run", atr_id);
    let mut running_sub = node.subscribe::<r2r::std_msgs::msg::Bool>
        (&running_topic, r2r::QosProfile::default())?;
    let shared_state_task = shared_state.clone();
    // TODO: move to function?
    tokio::task::spawn(async move {
        loop {
            if let Some(msg) = running_sub.next().await {
                (*shared_state_task.lock().unwrap()).running = msg.data;
            }
        }
    });

    // Update waypoint positions from scene manipulation service
    let lookup_service = node.create_client::<LookupTransform::Service>("/lookup_transform")?;
    let shared_state_task = shared_state.clone();
    let waiting = node.is_available(&lookup_service).unwrap();
    tokio::task::spawn(async move {
        waiting.await.unwrap();
        println!("Lookup transform service is available, start to fetch waypoints.");
        fetch_waypoints_tick(lookup_service, shared_state_task)
            .await
            .expect("fail");
    });



    // Set path service call
    use r2r::gpss_control_interfaces::srv::SetPath;
    let topic = format!("/atr_{}/set_path", atr_id);
    let shared_state_task = shared_state.clone();
    let mut service = node.create_service::<SetPath::Service>(&topic)?;
    tokio::task::spawn(async move {
        loop {
            match service.next().await {
                Some(req) => {
                    let msg = &req.message;
                    let mut ss = shared_state_task.lock().unwrap();
                    let path_string = msg.path.waypoints.iter()
                        .map(|wp| wp.id.clone())
                        .collect::<Vec<String>>().join(" -> ");

                    println!("Got new path: {}", path_string);

                    // TODO: check if we should accept new path etc.
                    ss.path_completed = false;
                    ss.to_visit = msg.path.waypoints.clone().into();
                    ss.current_waypoint = ss.to_visit.pop_front();
                    ss.current_waypoint_terminal = ss.to_visit.len() == 0;
                    ss.last_reached_waypoint = None;
                    ss.last_reached_waypoint_timestamp = None;

                    let resp = SetPath::Response {
                        accepted: true,
                    };

                    req.respond(resp).expect("could not send service response");
                }
                None => break,
            }
        }
    });

    // An ATR teleporter for controlling the simulator
    let teleport_topic = format!("/atr_{}/teleport", atr_id);
    let mut teleport_sub = node.subscribe::<Point>
        (&teleport_topic, r2r::QosProfile::default())?;
    let shared_state_task = shared_state.clone();
    // TODO: move to function?
    tokio::task::spawn(async move {
        loop {
            if let Some(msg) = teleport_sub.next().await {
                let mut ss = shared_state_task.lock().unwrap();
                println!("Teleport atr to x:{}, y:{}, from x:{}, y:{}", &msg.x, &msg.y, &ss.atr_pose.x, &ss.atr_pose.y);
                ss.atr_pose.x = msg.x;
                ss.atr_pose.y = msg.y;
            }
        }
    });


    let handle = tokio::task::spawn_blocking(move || loop {
        node.spin_once(std::time::Duration::from_millis(100));
    });

    // await spin task (will never finish)
    handle.await?;

    Ok(())
}


fn geometry_polygon_to_geo_polygon(poly: &r2r::geometry_msgs::msg::Polygon) -> geo::Polygon {
    // From geo docs: If a LineString’s first and last Coordinate have different values, a new Coordinate will be appended to the LineString with a value equal to the first Coordinate.
    // So we don't need to add more points.
    let points: Vec<geo::Coordinate<f64>> = poly.points.iter().map(|p| {
        geo::Coordinate::<f64> { x: p.x.into(), y: p.y.into() } // we ignore z
    }).collect();
    geo::Polygon::new(
        geo::LineString::from(points),
        vec![])
}

#[cfg(test)]
mod tests {
    use geo::Intersects;
    use geo::line_string;
    use geo::{LineString, Polygon};

    #[test]
    fn test_geo_library() {
        let line = line_string![
            (x: 0.0, y: 0.1),
            (x: 1.1, y: 1.1),
        ];

        let polygon = Polygon::new(
            LineString::from(vec![(0., 0.), (1., 1.), (1., 0.)]), //, (0., 0.)]),
            vec![],
        );

        assert!(line.intersects(&polygon));
    }
}
