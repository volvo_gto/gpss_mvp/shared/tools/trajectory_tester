use r2r;
use serde_json::Value;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Debug, Clone, Serialize, Deserialize)]
struct Node {
    id: i32,
    position: [f64;3],
    name: String,
    description: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
struct Waypoint {
    node: Node,
    radius: f64,  // the radius of a blend circle around x, y in m
    direction: f64, // if it is a stopping pose, this defines the angle of the robots final pose
}

fn read_node_list() -> Result<Vec<Node>, Box<dyn std::error::Error>> {
    let data = r#"
        {
            "Nodes":
            [
                {"id": 1,  "position":  [ 7.0, 2.08, 0.0], "name": "grid_1_1", "description": "grid_1_1"},
                {"id": 2,  "position":  [ 7.0, 3.08, 0.0], "name": "grid_1_2", "description": "grid_1_2"},
                {"id": 3,  "position":  [ 7.0, 4.08, 0.0], "name": "grid_1_3", "description": "grid_1_3"},
                {"id": 4,  "position":  [ 7.0, 5.08, 0.0], "name": "grid_1_4", "description": "grid_1_4"},
                {"id": 5,  "position":  [ 8.0, 2.08, 0.0], "name": "grid_2_1", "description": "grid_2_1"},
                {"id": 6,  "position":  [ 8.0, 3.08, 0.0], "name": "grid_2_2", "description": "grid_2_2"},
                {"id": 7,  "position":  [ 8.0, 4.08, 0.0], "name": "grid_2_3", "description": "grid_2_3"},
                {"id": 8,  "position":  [ 8.0, 5.08, 0.0], "name": "grid_2_4", "description": "grid_2_4"},
                {"id": 9,  "position":  [ 9.0, 2.08, 0.0], "name": "grid_3_1", "description": "grid_3_1"},
                {"id": 10,  "position": [ 9.0, 3.08, 0.0], "name": "grid_3_2", "description": "grid_3_2"},
                {"id": 11,  "position": [ 9.0, 4.08, 0.0], "name": "grid_3_3", "description": "grid_3_3"},
                {"id": 12,  "position": [ 9.0, 5.08, 0.0], "name": "grid_3_4", "description": "grid_3_4"},
                {"id": 13,  "position": [ 10.0, 2.08, 0.0], "name": "grid_4_1", "description": "grid_4_1"},
                {"id": 14,  "position": [ 10.0, 3.08, 0.0], "name": "grid_4_2", "description": "grid_4_2"},
                {"id": 15,  "position": [ 10.0, 4.08, 0.0], "name": "grid_4_3", "description": "grid_4_3"},
                {"id": 16,  "position": [ 10.0, 5.08, 0.0], "name": "grid_4_4", "description": "grid_4_4"},
                {"id": 17,  "position": [ 11.0, 2.08, 0.0], "name": "grid_5_1", "description": "grid_5_1"},
                {"id": 18,  "position": [ 11.00, 3.08, 0.0], "name": "grid_5_2", "description": "grid_5_2"},
                {"id": 19,  "position": [ 11.00, 4.08, 0.0], "name": "grid_5_3", "description": "grid_5_3"},
                {"id": 20,  "position": [ 11.00, 5.08, 0.0], "name": "grid_5_4", "description": "grid_5_4"}
            ],
            "Paths":
            [
                {"id": 1, "graph": ["grid_4_4", "grid_2_1"]}
            ]
        }
        "#;

    let data: Value = serde_json::from_str(&data)?;
    let nodes = &data["Nodes"];
    let nodes = serde_json::from_value(nodes.clone())?;

    Ok(nodes)
}

fn make_pose_path(node_data: &[Node]) -> Vec<Waypoint> {
    let n: HashMap<&str, &Node> = node_data.iter().map(|x| (x.name.as_str(), x)).collect();
    return vec![
        // Waypoint {node: n["grid_2_1"].clone(), radius: 1.0, direction: 0.0},
        // Waypoint {node: n["grid_2_4"].clone(), radius: 1.0, direction: 0.0},
        // Waypoint {node: n["grid_4_4"].clone(), radius: 1.0, direction: 0.0},
        // Waypoint {node: n["grid_4_1"].clone(), radius: 0.3, direction: 0.0},
        // Waypoint {node: n["grid_2_4"].clone(), radius: 1.0, direction: 0.0},

        // run twice...
        // Waypoint {node: n["grid_2_1"].clone(), radius: 1.0, direction: 0.0},
        // Waypoint {node: n["grid_2_4"].clone(), radius: 1.0, direction: 0.0},
        // Waypoint {node: n["grid_4_4"].clone(), radius: 1.0, direction: 0.0},
        Waypoint {node: n["grid_4_1"].clone(), radius: 0.3, direction: 0.0},
        Waypoint {node: n["grid_2_4"].clone(), radius: 1.0, direction: 0.0},
    ]
}


#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let ctx = r2r::Context::create()?;
    let mut node = r2r::Node::create(ctx, "testnode", "")?;

    use text_io::read;
    print!("Enter AGV id: ");
    let atr_id: i32 = read!();

    let nodes = read_node_list()?;
    let path = make_pose_path(&nodes);
    println!("path: {:#?}", path);

    use r2r::gpss_control_interfaces::srv::SetPath;
    let topic = format!("/atr_{}/set_path", atr_id);
    let client = node.create_client::<SetPath::Service>(&topic)?;
    let waiting = node.is_available(&client)?;

    let handle = tokio::task::spawn_blocking(move || loop {
        node.spin_once(std::time::Duration::from_millis(100));
    });

    println!("waiting for service...");
    waiting.await?;
    println!("service available.");

    // create request
    use r2r::gpss_control_interfaces::msg::Waypoint as RWP;
    use r2r::gpss_control_interfaces::msg::Path as RPath;
    let mut waypoints = vec![];
    for wp in &path {
        let point = r2r::geometry_msgs::msg::Point {
            x: wp.node.position[0],
            y: wp.node.position[1],
            z: wp.node.position[2],
        };
        let rwp = RWP {
            id: wp.node.name.to_owned(),
            point,
            zone: wp.radius,
            direction: wp.direction,
        };
        waypoints.push(rwp);
    }

    let path = RPath { waypoints };
    let req = SetPath::Request { path };
    if let Ok(resp) = client.request(&req)?.await {
        println!("accepted? {}", resp.accepted);
    }

    handle.await?;

    Ok(())
}
